const expect = require('chai').expect
const assert = require('chai').assert
const should = require('chai').should()
const mylib = require('../src/mylib')

describe('Unit testing mylib.js', () => {
    let myvar = undefined

    
    it('Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1,1)
        expect(result).to.equal(2)
    })
    
    it('Myvar should exist', () => {
        should.exist(myvar)
    })
    
    after(() => {
        console.log('Test log from after-function')
    })

    // This is executed before any other tests
    before(() => {
        console.log('Test log in before-function')
        myvar = 1
    })

    it.skip('Assert foo is not bar', () => {
        assert('foo' !== 'bar')
    })
})
